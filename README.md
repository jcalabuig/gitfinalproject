# Version Control with GIT - Final project

## Description

Create a Git repository with the commits shown in the commit graph and table below. This simulates a team building and releasing a product using the Gitflow workflow. The first release (v1.00) has only one feature. Soon after release, a bug was discovered and a hotfix (v1.01) was necessary.

## Authors

Jorge Calabuig Bartual (https://gitlab.com/jcalabuig)

## Version History

* 1.00
    * Has only one feature.
* 1.01
    * Hotfix.